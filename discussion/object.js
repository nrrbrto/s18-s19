//[SECTION] Objects

  //Objects -> is a collection of related data and/or functionalities. the purpose of an object is not only to store multiple  data-sets but also to represent real world objects.

  //SYNTAX: 
      // let/const variableName = {
      // 	key/property: value
      // }

      //Note: Information stored in objects are represented in key:value pairing. 

   //lets use this example to describe a real-world item/object
   let cellphone = {
   	 name: 'Nokia 3210',
   	 manufactureDate: '1999',
   	 price: 1000 
   }; //'key' -> is also mostly referred to as a 'property' of an object. 

   console.log(cellphone); 
   console.log(typeof cellphone); 

   //How to store multiple objects, 

   //you can use an array struture to store them.
   let users = [
	   { 
	   	 name: 'Anna', 
	   	 age: '23'
	   },
	   { 
	   	 name: 'Nicole', 
	   	 age: '18'
	   },
	   { 
	   	 name: 'Smith', 
	   	 age: '42'
	   },
	   { 
	   	 name: 'Pam', 
	   	 age: '13'
	   },
	   { 
	   	 name: 'Anderson', 
	   	 age: '26'
	   }
  	];

   console.log(users); 
   //now there is an alternative way when displaying values of an object collection
   console.table(users); 

   //Complex examples of JS objects

   //NOTE: Different data types may be stored in an object's property creating more complex data structures

   //When using JS Objects, you can also insert 'Methods'.

   //Methods -> are useful for creating reusable functions that can perform tasks related to an object. 
   let friend = {
   	 //properties
   	 firstName: 'Joe', //String or numbers
   	 lastName: 'Smith', 
   	 isSingle: true, //boolean
     emails: ['joesmith@gmail.com', 'jsmith@company.com'], //array
     address: { //Objects
     	city: 'Austin',
     	state: 'Texas',
     	country: 'USA'
     },
     //methods 
     introduce: function() {
     	console.log('Hello Meet my new Friend'); 
     },
     advice: function() {
     	console.log('Give friend an advice to move on');
     }, 
     wingman: function() {
     	console.log('Hype friend when meeting new people');
     }
   }
   console.log(friend);

   //[SECTION] ALTERNATE APPROACHES on HOW TO DECLARE AN OBJECT IN JAVASCRIPT

   	//1. How to create an Object using a constructor function?
   	// Contructor function -> we will need to create a reusable function in which we will declare a bluepring to describe the anatomy of an objects we wish to create

   	//to declare properties within a constructor function, you have to be familiar with the 'this' keyword
   	function Laptop(name, year, model){
   		this.name = name;
   		this.manufactredOn = year;
   		this.model = model;
   	}

   	let laptop1 = new Laptop ('Sony', 2008, 'viao')
   	console.log(laptop1)
   	//==> use and benefits
   	let laptop2 = new Laptop ('Apple', 2019, 'Mac')
   	console.log(laptop2)
   	let laptop3 = new Laptop ('HP', 2015, 'hp')
   	console.log(laptop3)
   	let gadgets = [laptop1, laptop2, laptop3]
   	console.table(gadgets)

   	function Pokemon (name, type, level, trainer){
   		this.pokemonName = name;
   		this.pokemonType = type;
   		this.pokemonHealth = 2 * level;
   		this.pokemonLevel = level;
   		this.owner = trainer;

   		this.tackle = function(target){
   			console.log(this.pokemonName + " tackled " + target.pokemonName)
   		};

   		this.greetings = function() {
   			console.log(this.pokemonName + ' says Hello! ')
   		};
   	}

   	let pikachu = new Pokemon('Pikachu', 'electric', 3, 'Ash Ketchup')
   	let ratata = new Pokemon('Ratata', 'normal', 4, 'Misty')
   	let snorlax = new Pokemon('Snorlax', 'normal', 5, 'Gary Oak')

   	let pokemons = [pikachu, ratata, snorlax]
   	console.table(pokemons)

//[Section] How to access Object Properties?

//using (.)

   	console.log(snorlax.owner)
   	console.log(pikachu.pokemonType)

pikachu.tackle(ratata)
snorlax.greetings();

//alternate using []
console.log(ratata['owner'])
console.log(ratata['pokemonLevel'])

//dot or square bracket?

//use to when accessing properties.

//additional knowledge
//you can write and declare objects in this way:


//to create objects, use object literals '{}'

let trainer = {}; //empty object
console.log(trainer)

//you can add properties outside the object

trainer.name = 'Ash Ketchup'
console.log(trainer)
trainer.friends = ['Misty', 'Brock', 'Tracey']
console.log(trainer)

//method
trainer.speak = function() {
	console.log('Pikachu, I choose you!')

	trainer.speak();
}